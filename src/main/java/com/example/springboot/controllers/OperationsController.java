package com.example.springboot.controllers;

import com.example.springboot.models.Wallet;
import com.example.springboot.repository.memory.CurrencyStorage;
import com.example.springboot.repository.memory.ExchangeRateStorage;
import com.example.springboot.repository.memory.WalletStorage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.UUID;

@Api(value = "Operations controller", description = "Used to handle financial operations")
@RestController
@RequestMapping("/operations")
public class OperationsController {
    private final static String INVALID_CURRENCY = "Invalid currency provided";
    private final static String INVALID_ACCOUNT = "Invalid account";
    private final static String INSUFFICIENT_FUNDS = "Insufficient funds";
    private final static String TRANSACTION_SUCCESSFUL = "Transaction successful";

    @ApiOperation(value = "Exchange an amount of source currency into a given target currency", response = String.class, tags = "operations")
    @GetMapping("/exchange")
    public String exchange(
            @RequestParam("sourceCurrency") String sourceCurrency,
            @RequestParam("targetCurrency") String targetCurrency,
            @RequestParam("ammount") double amount,
            @RequestParam("wallet") UUID walletID
    ) throws Exception {
        Wallet wallet = WalletStorage.getWallet(walletID);
        if (CurrencyStorage.getCurrencyByKey(sourceCurrency) == null
                || CurrencyStorage.getCurrencyByKey(targetCurrency) == null) {
            throw new Exception(INVALID_CURRENCY);
        }
        if (wallet == null){
            throw new Exception(INVALID_ACCOUNT);
        }

        double exchangeRate = ExchangeRateStorage.getExchangeRate(sourceCurrency, targetCurrency, true);

        synchronized (wallet) {
            if (wallet.getAmmounts().get(sourceCurrency) < amount)
                throw new Exception(INSUFFICIENT_FUNDS);
            HashMap<String, Double> ammountMap = new HashMap<>();
            ammountMap.put(sourceCurrency, amount * -1);
            ammountMap.put(targetCurrency, amount * exchangeRate);
            wallet.updateAmmounts(ammountMap);
        }

        return TRANSACTION_SUCCESSFUL;
    }

    @ApiOperation(value = "Transfer an amount of source currency into a given target wallet, exchanging it to target currency",
            response = String.class, tags = "operations")
    @GetMapping("/transfer")
    public String transfer(
            @RequestParam("sourceCurrency") String sourceCurrency,
            @RequestParam("targetCurrency") String targetCurrency,
            @RequestParam("fromWallet") UUID fromAccount,
            @RequestParam("toWallet") UUID toAccount,
            @RequestParam("amount") double amount
    ) throws Exception {
        Wallet from = WalletStorage.getWallet(fromAccount);
        Wallet to = WalletStorage.getWallet(toAccount);
        if (CurrencyStorage.getCurrencyByKey(sourceCurrency) == null
                || CurrencyStorage.getCurrencyByKey(targetCurrency) == null){
            throw new Exception(INVALID_CURRENCY);
        }
        if ( from == null || to == null) {
            throw new Exception(INVALID_ACCOUNT);
        }
        double exchangeRate = ExchangeRateStorage.getExchangeRate(sourceCurrency, targetCurrency, true);

        synchronized (from) {
            if (from.getAmmounts().get(sourceCurrency) < amount) {
                throw new Exception(INSUFFICIENT_FUNDS);
            }
            HashMap<String, Double> amountMap = new HashMap<>();
            amountMap.put(sourceCurrency, amount * -1);
            from.updateAmmounts(amountMap);
        }

        synchronized (to) {
            HashMap<String, Double> amountMap = new HashMap<>();
            amountMap.put(targetCurrency, amount * exchangeRate);
            to.updateAmmounts(amountMap);
        }

        return TRANSACTION_SUCCESSFUL;
    }

}
