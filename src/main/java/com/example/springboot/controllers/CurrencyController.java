package com.example.springboot.controllers;

import com.example.springboot.models.ExchangeRate;
import com.example.springboot.repository.memory.ExchangeRateStorage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

@Api(value = "Currency controller", description = "Used to retrieve exchange rates")
@RestController
public class CurrencyController {

    @ApiOperation(value = "Get list of exchange rate pairs", response = Iterable.class, tags = "rates")
    @GetMapping("/rates")
    public List<ExchangeRate> index(
            @RequestParam("page") Optional<Integer> oPage,
            @RequestParam("pageSize") Optional<Integer> oPageSize
    ) throws IOException, URISyntaxException {
        return ExchangeRateStorage.getExchangeRateList(oPage.orElse(0), oPageSize.orElse(10));
    }

}
