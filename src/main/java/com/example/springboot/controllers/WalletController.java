package com.example.springboot.controllers;

import com.example.springboot.repository.memory.WalletRepository;
import com.example.springboot.repository.memory.WalletStorage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import com.example.springboot.models.Wallet;

import java.util.Collection;
import java.util.UUID;

@Api(value = "Wallet controller", description = "Used to perform CRUD operations on wallets")
@RestController
@RequestMapping("/wallets")
public class WalletController {

    @ApiOperation(value = "Returns a list of all existing wallets",
            response = Iterable.class, tags = "wallets")
    @GetMapping("/")
    public Collection<Wallet> getWallets() {
        return new WalletRepository().findAll();
    }

    @ApiOperation(value = "Returns the wallet based on uuid",
            response = Wallet.class, tags = "wallets")
    @GetMapping("/{id}")
    Wallet getWallet(@PathVariable UUID id) {
        return new WalletRepository().find(id);
    }

    @ApiOperation(value = "Creates a new wallet",
            response = Wallet.class, tags = "wallets")
    @PostMapping(value = "/", consumes = "application/json", produces = "application/json")
    Wallet newWallet(@RequestBody Wallet wallet) throws Exception {
        return new WalletRepository().add(wallet);
    }

    /*
        the put mapping for the currency is adding or substracting the sum, the real value should be calculated on the server
     */
    @ApiOperation(value = "Updates wallet specified by UUID",
            response = Wallet.class, tags = "wallets")
    @PutMapping("/{id}")
    Wallet updateWallet(@RequestBody Wallet wallet, @PathVariable UUID id) throws Exception {
        wallet.setIdentifier(id);
        return new WalletRepository().edit(wallet);
    }

    @ApiOperation(value = "Deletes wallet specified by UUID",
            response = String.class, tags = "wallets")
    @DeleteMapping("/{id}")
    String deleteWallet(@PathVariable UUID id) throws Exception {
        return new WalletRepository().delete(id);
    }
}
