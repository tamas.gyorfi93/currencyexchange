package com.example.springboot.models;

public class ExchangeRate implements Model {
    private String firstCurrency;
    private String secondCurrency;
    private double exchangeRate;

    public ExchangeRate() {

    }

    public ExchangeRate(String first, String second, double rate) {
        this.firstCurrency = first;
        this.secondCurrency = second;
        this.exchangeRate = rate;
    }

    public String getFirstCurrency() {
        return firstCurrency;
    }

    public void setFirstCurrency(String firstCurrency) {
        this.firstCurrency = firstCurrency;
    }

    public String getSecondCurrency() {
        return secondCurrency;
    }

    public void setSecondCurrency(String secondCurrency) {
        this.secondCurrency = secondCurrency;
    }

    public double getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(double exchangeRate) {
        this.exchangeRate = exchangeRate;
    }
}
