package com.example.springboot.models;

import com.example.springboot.repository.memory.CurrencyStorage;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.UUID;

public class Wallet implements Model {
    private String displayName;
    private UUID identifier;
    private HashMap<String, Double> ammounts = new HashMap<>();

    public Wallet() {
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public UUID getIdentifier() {
        return identifier;
    }

    public void setIdentifier(UUID identifier) {
        this.identifier = identifier;
    }

    public HashMap<String, Double> getAmmounts() {
        return ammounts;
    }

    public void setAmmounts(HashMap<String, Double> changes) throws Exception {
        for (Entry<String, Double> e : changes.entrySet()) {
            if (CurrencyStorage.getCurrencyByKey(e.getKey()) == null) {
                throw new Exception("Invalid Currency: " + e.getKey());
            }
        }
        for (Entry<String, Double> e : changes.entrySet()) {
            ammounts.put(e.getKey(), e.getValue());
        }
    }


    public void updateAmmounts(HashMap<String, Double> changes) throws Exception {
        for (Entry<String, Double> e : changes.entrySet()) {
            if (CurrencyStorage.getCurrencyByKey(e.getKey()) == null) {
                throw new Exception("Invalid Currency: " + e.getKey());
            }
        }
        for (Entry<String, Double> e : changes.entrySet()) {
            if (ammounts.containsKey(e.getKey())) {
                if (e.getValue() < 0 && ammounts.get(e.getKey()) < (-1 * e.getValue())) {
                    throw new Exception("Insufficient funds in currency: " + e.getKey());
                }
                ammounts.put(e.getKey(), ammounts.get(e.getKey()) + e.getValue());
            } else {
                if (e.getValue() > 0) {
                    ammounts.put(e.getKey(), e.getValue());
                } else {
                    throw new Exception("Can not initialize with negative ammount for currency: " + e.getKey());
                }
            }
        }
    }

    public Wallet update(Wallet wallet) throws Exception {
        setDisplayName(wallet.getDisplayName());
        updateAmmounts(wallet.getAmmounts());
        return this;
    }

    @Override
    public boolean equals(Object wallet) {
        return getIdentifier().equals(((Wallet) wallet).getIdentifier());
    }
}
