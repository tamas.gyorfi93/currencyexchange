package com.example.springboot.services;

import com.example.springboot.models.ExchangeRate;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.LinkedList;

public class ExchangeService {

    static CloseableHttpClient httpClient = HttpClients.createDefault();

    public static LinkedList<ExchangeRate> getRates(String sourceCurrency, String targetCurrencies) throws URISyntaxException, IOException {
        URIBuilder builder = new URIBuilder("https://min-api.cryptocompare.com/data/price");
        builder.setParameter("fsym", sourceCurrency);
        builder.setParameter("tsyms", targetCurrencies);

        HttpGet request = new HttpGet(builder.build());
        CloseableHttpResponse response = httpClient.execute(request);

        try {
            LinkedList<ExchangeRate> rates = new LinkedList<>();
            if (response.getStatusLine().getStatusCode() == 200) {
                HttpEntity entity = response.getEntity();
                if (entity != null) {
                    String result = EntityUtils.toString(entity);
                    System.out.println(result);
                    JSONObject values = new JSONObject(result);
                    for (String s : values.keySet()) {
                        rates.add(new ExchangeRate(sourceCurrency, s, values.getDouble(s)));
                        System.out.println(sourceCurrency + " to " + s + " : " + String.format("%.18f", values.getDouble(s)));
                    }

                    return rates;
                }
            }
        } catch (IOException e) {
            throw e;
        } finally {
            response.close();
        }
        return null;
    }

}
