package com.example.springboot.repository;

import com.example.springboot.models.Model;

import java.util.Collection;
import java.util.UUID;

public interface Repository<T extends Model > {
    public Collection<T> findAll();
    public T find(UUID id);
    public T add(T model) throws Exception;
    public T edit(T model) throws Exception;
    public String delete(UUID id) throws Exception;
}
