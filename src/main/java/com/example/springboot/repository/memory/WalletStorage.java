package com.example.springboot.repository.memory;

import com.example.springboot.models.Wallet;

import java.util.Collection;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class WalletStorage {

    private static ConcurrentHashMap<UUID, Wallet> wallets = new ConcurrentHashMap<>();

    public static Wallet createWallet(Wallet w) {
        w.setIdentifier(UUID.randomUUID());
        while (w.getIdentifier() == null || wallets.containsKey(w.getIdentifier())) {
            w.setIdentifier(UUID.randomUUID());
        }
        wallets.put(w.getIdentifier(), w);
        return w;
    }

    public static Wallet updateWallet(Wallet w) throws Exception {
        if (wallets.containsKey(w.getIdentifier())) {
            wallets.put(w.getIdentifier(), wallets.get(w.getIdentifier()).update(w));
        } else {
            throw new Exception("Could not find wallet with id: " + w.getIdentifier());
        }

        return w;
    }

    public static Wallet getWallet(UUID id) {
        return wallets.get(id);
    }

    public static Collection<Wallet> getWallets() {
        return wallets.values();
    }


    public static String deleteWallet(UUID id) throws Exception {
        if (wallets.containsKey(id)) {
            wallets.remove(id);
            return "Deleted wallet with id: " + id;
        }
        throw new Exception("Could not find wallet with id: " + id);
    }

}
