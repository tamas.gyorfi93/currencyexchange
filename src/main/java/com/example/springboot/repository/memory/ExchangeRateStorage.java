package com.example.springboot.repository.memory;

import com.example.springboot.models.ExchangeRate;
import com.example.springboot.services.ExchangeService;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class ExchangeRateStorage {
    private static boolean updating = false;
    public static List<ExchangeRate> rates = new LinkedList<>();
    private static long lastTimeStamp = System.currentTimeMillis();


    public static List<ExchangeRate> getExchangeRateList(int page, int pageSize) throws IOException, URISyntaxException {
        updateRates(false);
        int firstIndex = page * pageSize;
        if (firstIndex >= rates.size()) {
            return new LinkedList<>();
        }
        int secondIndex = (page + 1) * pageSize;
        secondIndex = Math.min(secondIndex, rates.size());
        return rates.subList(firstIndex, secondIndex);
    }

    private static void updateRates(boolean forceUpdate) throws IOException, URISyntaxException {
        if (!forceUpdate) {//unless it is an exchange, we cache the list of currencies for a minute
            if (updating || !rates.isEmpty() && System.currentTimeMillis() - lastTimeStamp < 60000) {
                return;
            }
        }
        updating = true;
        LinkedList<ExchangeRate> newRates = new LinkedList<>();
        Collection<String> supportedCurrencies = CurrencyStorage.getSupportedCurrencyCodes();
        for (String sourceCurrency : supportedCurrencies) {
            newRates.addAll(ExchangeService.getRates(sourceCurrency, String.join(",", supportedCurrencies)));
        }
        rates = newRates;
        lastTimeStamp = System.currentTimeMillis();
        updating = false;
    }

    public static double getExchangeRate(String sourceCurrency, String targetCurrency, boolean force) throws IOException, URISyntaxException {
        updateRates(force);
        for (ExchangeRate er : rates) {
            if (er.getFirstCurrency().equalsIgnoreCase(sourceCurrency) && er.getSecondCurrency().equalsIgnoreCase(targetCurrency)) {
                return er.getExchangeRate();
            }
        }
        return 1.0;
    }

}
