package com.example.springboot.repository.memory;

import com.example.springboot.models.Wallet;
import com.example.springboot.repository.Repository;

import java.util.Collection;
import java.util.UUID;

public class WalletRepository implements Repository<Wallet> {
    @Override
    public Collection<Wallet> findAll() {
        return WalletStorage.getWallets();
    }

    @Override
    public Wallet find(UUID id) {
        return WalletStorage.getWallet(id);
    }

    @Override
    public Wallet add(Wallet model) throws Exception {
        return WalletStorage.createWallet(model);
    }

    @Override
    public Wallet edit(Wallet model) throws Exception {
        return WalletStorage.updateWallet(model);
    }

    @Override
    public String delete(UUID id) throws Exception {
        return WalletStorage.deleteWallet(id);
    }
}
