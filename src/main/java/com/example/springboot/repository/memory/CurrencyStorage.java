package com.example.springboot.repository.memory;

import com.example.springboot.models.Currency;

import java.util.Collection;
import java.util.HashMap;

public class CurrencyStorage {
    private static HashMap<String, Currency> currencies = new HashMap<>();

    static {
        Currency usd = new Currency();
        usd.setDisplayName("United States Dollar");
        usd.setCurrencyCode("USD");
        currencies.put(usd.getCurrencyCode(), usd);

        Currency eur = new Currency();
        eur.setDisplayName("Euro");
        eur.setCurrencyCode("EUR");
        currencies.put(eur.getCurrencyCode(), eur);

        Currency ron = new Currency();
        ron.setDisplayName("Romanian Leu");
        ron.setCurrencyCode("RON");
        currencies.put(ron.getCurrencyCode(), ron);

        Currency rub = new Currency();
        rub.setDisplayName("Russian Ruble");
        rub.setCurrencyCode("RUB");
        currencies.put(rub.getCurrencyCode(), rub);

        Currency jpy = new Currency();
        jpy.setDisplayName("Japanese Yen");
        jpy.setCurrencyCode("JPY");
        currencies.put(jpy.getCurrencyCode(), jpy);

        Currency btc = new Currency();
        btc.setDisplayName("BitCoin");
        btc.setCurrencyCode("BTC");
        currencies.put(btc.getCurrencyCode(), btc);

        Currency ltc = new Currency();
        ltc.setDisplayName("LiteCoin");
        ltc.setCurrencyCode("LTC");
        currencies.put(ltc.getCurrencyCode(), ltc);

        Currency etc = new Currency();
        etc.setDisplayName("Ethereum");
        etc.setCurrencyCode("ETC");
        currencies.put(etc.getCurrencyCode(), etc);

        Currency rip = new Currency();
        rip.setDisplayName("Ripple");
        rip.setCurrencyCode("XRP");
        currencies.put(rip.getCurrencyCode(), rip);

        Currency mon = new Currency();
        mon.setDisplayName("Monero");
        mon.setCurrencyCode("XMR");
        currencies.put(mon.getCurrencyCode(), mon);
    }

    public static Currency getCurrencyByKey(String currencyCode) {
        return currencies.get(currencyCode);
    }

    public static Collection<String> getSupportedCurrencyCodes() {
        return currencies.keySet();
    }
}
